#each bus has an ID that indicates how often it leaves

#at timestamp 0, every bus left the port

#bus with ID 5 leaves at timestamp 0, 5, 10 etc.

#input: line 1 is the earliest timestamp when you can be at the bus station,
#line 2 is a list of buses that are in service

shuttles = open(r"C:\Python34\Advent of Code 2020\Day13 - shuttle_search\shuttles.txt", "r")

line = shuttles.readline()
line = shuttles.readline()
line = line.rstrip("\n")
linelist = line.split(",")

print(linelist)

shuttledic = {}
buslines = []

for index in range(len(linelist)):
    if linelist[index] != "x":
        #key = bus ID, value = delay after first
        shuttledic[int(linelist[index])] = index
        buslines.append(int(linelist[index]))

print(shuttledic)
print(buslines)

base_time = buslines[0]
counting_from = buslines[0]
print(base_time)

def get_timestamp(busID):
    step = base_time
    delay = shuttledic[busID]
    counter = 1
    timestamp = 0
    while ((counter * base_time) + delay + counting_from) % busID != 0:
        counter += 1
    else:
        timestamp = (counter * base_time) + counting_from
        return timestamp

for index in range(1, len(buslines)):
    busID = buslines[index]
    timestamp = get_timestamp(busID)
    print("timestamp is:", timestamp)
    base_time = base_time * busID
    counting_from = timestamp

    
