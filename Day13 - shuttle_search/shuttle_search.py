#each bus has an ID that indicates how often it leaves

#at timestamo 0, every bus left the port

#bus with ID 5 leaves at timestamp 0, 5, 10 etc.

#input: line 1 is the earliest timestamp when you can be at the bus station,
#line 2 is a list of buses that are in service

shuttles = open(r"C:\Python34\Advent of Code 2020\Day13 - shuttle_search\shuttles.txt", "r")

line = shuttles.readline()
line.rstrip("\n")
timestamp = int(line)
print("Timestamp is:", timestamp)

line = shuttles.readline()
line = line.rstrip("\n")
linelist = line.split(",")

shuttlelist = []
for index in range(len(linelist)):
    if linelist[index] != "x":
        shuttlelist.append(int(linelist[index]))

shuttlelist.sort()
print("Shuttles in service are:", shuttlelist)

shuttletimes = []
for bus in shuttlelist:
    waiting = bus - (timestamp % bus)
    shuttletimes.append(str(waiting) + " minutes waiting for bus " + str(bus))

shuttletimes.sort()
print(shuttletimes)
