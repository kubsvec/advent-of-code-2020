#each bus has an ID that indicates how often it leaves

#at timestamp 0, every bus left the port

#bus with ID 5 leaves at timestamp 0, 5, 10 etc.

#input: line 1 is the earliest timestamp when you can be at the bus station,
#line 2 is a list of buses that are in service

shuttles = open(r"C:\Python34\Advent of Code 2020\Day13 - shuttle_search\testfile.txt", "r")

line = shuttles.readline()
line = shuttles.readline()
line = line.rstrip("\n")
linelist = line.split(",")

shuttlelist = []
for item in linelist:
    if item != 'x':
        shuttlelist.append(int(item))

print(linelist)

isvalid = False
time = min(shuttlelist)

while isvalid == False:
    inner = True
    for index in range(len(linelist)):
        if linelist[index] != 'x':
             if (time + index) % int(linelist[index]) != 0:
                 inner = False
    if inner == True:
        print("Found the right time at timestamp:", time)
    isvalid = inner
    time += min(shuttlelist)

def repeat(time):
    isvalid = False
    while isvalid == False:
        inner = True
        for index in range(len(linelist)):
            if linelist[index] != 'x':
                 if (time + index) % int(linelist[index]) != 0:
                     inner = False
        if inner == True:
            print("Found the right time at timestamp:", time)
        isvalid = inner
        time += min(shuttlelist)
