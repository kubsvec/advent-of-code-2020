def count_valid_passwords(my_file):
    """ (file open for reading) -> int

    Precondition: the file consists of lines where each line begins with the allowed
    number of characters in form of int-int, then has a blank space, then the character,
    a colon, a blank space and the password.

    Return the number of passwords in given file that meet the criteria for validity:
    a password is valid when it does not contain more or less of the given character than
    specified by the criteria.

    1-3 a: abcde
    1-3 b: cdefg
    2-9 c: ccccccccc
    >>> count_valid_passwords(my_file):
    2
    (the first password is valid for it contains between 1-3 "a" characters, the second is invalid,
    the third is valid)
    """
    #first we set the amount of valid passwords to 0
    valid_amount = 0
    #now we check every line in the file
    for line in my_file:
        #we need to isolate the criteria range, the criteria character and the password.
        #we will use the partition function to get a tuple with parts of the line in it while
        #removing the \n at the end of the line
        partition1 = line.rstrip("\n").partition(" ")
        #we get something like this ('1-3', ' ', 'b: cdefg') for the criteria range and will use
        #partition again to get the individual numbers
        partition2 = partition1[0].partition("-")
        #and get this ('1', '-', '3')
        #then we will extract the password
        partition3 = line.rstrip("\n").rpartition(" ")
        #by getting this ('1-3 b:', ' ', 'cdefg')
        #now we check if the password is valid by accessing the isolated parts of the line and if
        #yes, we increase the valid_amount by 1
        if (partition3[2].count(partition1[2][0]) >= int(partition2[0])) and(partition3[2].count(partition1[2][0]) <= int(partition2[2])):
            valid_amount += 1
    return valid_amount

def recount_valid_passwords(my_file):
    """ (file open for reading) -> int

    Precondition: the file consists of lines where each line begins with the allowed
    number of characters in form of int-int, then has a blank space, then the character,
    a colon, a blank space and the password.

    Return the number of passwords in given file that meet the criteria for validity:
    a password is valid when the given character is either at the first given position
    or at the second given position in the password (not at both!) specified by the crtiteria.

    1-3 a: abcde
    1-3 b: cdefg
    2-9 c: ccccccccc
    >>> recount_valid_passwords(my_file):
    1
    (The first password is valid for it contains "a" at position 1 and not at position 3. The
    second is invalid because it does not contain "b" at all and the third is invalid because "c"
    is at both the second and the ninth osition in the password.)
    """
    #first we set the amount of valid passwords to 0
    valid_amount = 0
    #now we check every line in the file
    for line in my_file:
        #we need to isolate the criteria, the criteria character and the password.
        #we will use the partition function to get a tuple with parts of the line in it while
        #removing the \n at the end of the line
        partition1 = line.rstrip("\n").partition(" ")
        #we get something like this ('1-3', ' ', 'b: cdefg') for the criteria range and the character
        character = partition1[2][0]
        #and we will use partition again to get the individual numbers
        partition2 = partition1[0].partition("-")
        #and get this ('1', '-', '3')
        criteria1 = int(partition2[0])
        criteria2 = int(partition2[2])
        #then we will extract the password
        partition3 = line.rstrip("\n").rpartition(" ")
        #by getting this ('1-3 b:', ' ', 'cdefg')
        password = partition3[2]
        #now we check if the password is valid by accessing the isolated parts of the line and if
        #yes, we increase the valid_amount by 1
        if (password[(criteria1 - 1)] == character and password[(criteria2 - 1)] != character) or (password[(criteria2 - 1)] == character and password[(criteria1 - 1)] != character):
            valid_amount += 1
    return valid_amount
