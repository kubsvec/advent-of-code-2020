data = open(r"C:\Python34\Advent of Code 2020\Day14 - ferry_docking\data.txt", "r")
line = data.readline()

mask = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
mems = {}

while line != "":
    if line.startswith("mask"):
        mask = line[7:]
        line = data.readline()
    else:
        line = line.rstrip("\n")
        linelist = line.split("] = ")
        memory_key = linelist[0][4:]
        value = int(linelist[1])
        value = bin(value)
        value = value[2:]
        value = value.rjust(36, "0")
        new_value = ""
        for index in range(0, 36):
            if mask[index] == "X":
                new_value = new_value + value[index]
            else:
                new_value = new_value + mask[index]
        memory_value = int(new_value, 2)
        mems[memory_key] = memory_value
        line = data.readline()
else:
    print(mems)

result = 0
for key in mems:
    result += mems[key]
print(result)
