data = open(r"C:\Python34\Advent of Code 2020\Day14 - ferry_docking\data.txt", "r")
line = data.readline()

mask = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
mems = {}

def replace_X(mystring):
    #fills an existing stringlist with 2 strings, where the first "X" was replaced by 1 and by 0
    original = mystring
    replacedone = False
    replacedzero = False
    onestring = ""
    zerostring = ""
    for char in original:
        if char == "X" and replacedone == False:
            onestring = onestring + "1"
            replacedone = True
        else:
            onestring = onestring + char
    for char in original:
        if char == "X" and replacedzero == False:
            zerostring = zerostring + "0"
            replacedzero = True
        else:
            zerostring = zerostring + char
    if onestring.count("X") > 0:
        replace_X(onestring)
    else:
        stringlist.append(onestring)
    if zerostring.count("X") > 0:
        replace_X(zerostring)
    else:
        stringlist.append(zerostring)

while line != "":
    if line.startswith("mask"):
        mask = line[7:]
        line = data.readline()
    else:
        line = line.rstrip("\n")
        linelist = line.split("] = ")
        memory_value = int(linelist[1])
        key = linelist[0][4:]
        key = int(key)
        key = bin(key)
        key = key[2:]
        key = key.rjust(36, "0")
        new_key = ""
        for index in range(0, 36):
            if mask[index] == "X":
                new_key = new_key + "X"
            if mask[index] == "1":
                new_key = new_key + "1"
            if mask[index] == "0":
                new_key = new_key + key[index]
        stringlist = []
        replace_X(new_key)
        for index in range(len(stringlist)):
            mems[stringlist[index]] = memory_value
        line = data.readline()
else:
    result = 0
    for key in mems:
        result += mems[key]
    print(result)
