#open file
customs = open(r"C:\Python34\AoC\custom_declarations.txt", "r")
#get our first line
line = customs.readline()
#create a list of all items that we are looking for
alphabet_string = 'abcdefghijklmnopqrstuvwxyz'
alphabet = []
for letter in alphabet_string:
    alphabet.append(letter)

group_list = []
people_in_group = 0
total_yes_answers = 0

while line != "":
    if line != ("\n" or ""):
        for index in range(len(line.rstrip("\n"))):
            group_list.append(line[index])
        people_in_group += 1
        line = customs.readline()
##        print("list updated to", group_list)
    else:
##        print("evaluating list", group_list)
        yes_answers = 0
        for item in alphabet:
            if group_list.count(item) == people_in_group:
                yes_answers += 1
        total_yes_answers += yes_answers
        line = customs.readline()
        people_in_group = 0
        group_list = []

yes_answers = 0
for item in alphabet:
    if group_list.count(item) == people_in_group:
        yes_answers += 1
total_yes_answers += yes_answers
print(total_yes_answers)
