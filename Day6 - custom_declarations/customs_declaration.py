#open file
customs = open(r"C:\Python34\AoC\custom_declarations.txt", "r")
#get our first line
line = customs.readline()
#create a list of all items that we are looking for
alphabet_string = 'abcdefghijklmnopqrstuvwxyz'
alphabet = []
for letter in alphabet_string:
    alphabet.append(letter)

group_dic = {}
total_yes_answers = 0

while line != "":
    if line != "\n":
        for index in range(len(line.rstrip("\n"))):
            group_dic[line[index]] = 1
        line = customs.readline()
    else:
        yes_answers = len(group_dic)
        total_yes_answers += yes_answers
        line = customs.readline()
        group_dic = {}

#count in the last group dic
yes_answers = len(group_dic)
total_yes_answers += yes_answers

print(total_yes_answers)
