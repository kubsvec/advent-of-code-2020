numbers = open(r"C:\Python34\Advent of Code 2020\Day9 - preamble_numbers\numbers.txt", "r")
numberlist = []
for line in numbers:
    numberlist.append(int(line.strip()))

preambule_amount = 25

def isvalidnumber(anumber, alist):
    #checks if anumber is the sum of any two numbers in alist
    results = []
    isvalid = False
    for index in range(len(alist)):
        for notindex in range(len(alist)):
            if index != notindex:
                asum = alist[index] + alist [notindex]
                results.append(asum)
    if anumber in results:
        isvalid = True
    return isvalid

notvalidnumbers = []

def arenotvalidnumbers(numlist):
    #returns a list of numbers from input list, that are not valid
     for index in range((preambule_amount), len(numlist)):
        mynumber = numlist[index]
        mylist = numlist[(index - preambule_amount):(index)]
        check = isvalidnumber(mynumber, mylist)
        if check == False:
            notvalidnumbers.append(mynumber)
     print("List of not valids (notvalidnumbers):\n", notvalidnumbers)

arenotvalidnumbers(numberlist)

firstinvalid = notvalidnumbers[0]

def findcontiguous(anumber, alist):
    #finds a contiguous set of numbers in alist that produce anumber when summed together
    for index in range(len(alist)):
        accumulator = alist[index]
        contiguouslist = [alist[index]]
        for notindex in range((index + 1), len(alist)):
            nextnumber = alist[notindex]
            result = accumulator + nextnumber
            contiguouslist.append(nextnumber)
            if anumber == result:
                print("Our contiguous set is", contiguouslist)
                solution = min(contiguouslist) + max(contiguouslist)
                print("The solution is", solution)
            accumulator = result

findcontiguous(firstinvalid, numberlist)

