#we open our file
passes = open(r"C:\Python34\AoC\boarding_passes.txt", "r")
#we read the first line and save it
line = passes.readline()

#we create a blank list for acquired seat_IDs
seat_ID_list = []
passes_checked = 0

#now we write the main code for every line of the file
while line != "":
    #every line has first 7 characters "F" or "B" representing rows and last 3 characters "R" or "L" representing columns
    #the whole row range is range(0, 128), the whole columns range is range(0, 7)
    #F accesses the lower half of active segment, B the upper half; L accesses the lower half, R the upper half
    row = range(0, 128)
    for index in range(0, 7):
        if line[index] == "F":
            row = range(row[0], row[int(len(row) / 2)])
        if line[index] == "B":
            row = range(row[int(len(row) / 2)], (row[-1] + 1))
##        print("reduced row segment to", row)
    final_row = row[0]
    print("the row is", final_row)
    column = range(0, 8)
    for index in range(7, 10):
        if line[index] == "L":
            column = range(column[0], column[int(len(column) / 2)])
        if line[index] == "R":
            column = range(column[int(len(column) / 2)], (column[-1] + 1))
##        print("reduced column segment to", column)
    final_column = column[0]
    print("the column is", final_column)
    seat_ID = (final_row * 8) + final_column
    seat_ID_list.append(seat_ID)
    print("seat ID is", seat_ID, "and was added to the list")
    passes_checked += 1
    line = passes.readline()

print("Checked", passes_checked, "boarding passes in total. Here is the list of their IDs.")
seat_ID_list.sort()
print(seat_ID_list, "\n")

print("the range in which we are looking for our seat is", range(seat_ID_list[0], (seat_ID_list[-1] + 1)))
for seat in range(seat_ID_list[0], (seat_ID_list[-1] + 1)):
    if seat not in seat_ID_list:
        print("The only missing seat is the seat with ID " + str(seat) + ". Have a pleasant flight!")

#done
