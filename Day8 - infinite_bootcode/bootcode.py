bootcode = open(r"C:\Python34\Advent of Code 2020\Day8 - infinite_bootcode\bootcode.txt", "r")
bootcode_list = bootcode.readlines()

def run_bootcode(mylist):
    index = 0
    accumulator = 0
    accessed_lines = []
    while index not in accessed_lines:
        splitted = bootcode_list[index].split()
        action = splitted[0]
        value = int(splitted[1])
        accessed_lines.append(index)
        if action == "nop":
            index += 1
        elif action == "acc":
            index += 1
            accumulator += value
        elif action == "jmp":
            index = index + value
    else:
        print("The accumulator before accessing a line for the 2nd time is ", accumulator, "\n\n")

run_bootcode(bootcode_list)

nops_evaluated = 0
jmps_evaluated = 0
program_terminated = []
attempt_number = 1
max_attempts = len(bootcode_list)

def debug_bootcode_nop(mylist):
    index = 0
    accumulator = 0
    accessed_lines = []
    nop_count = 0
    while index not in accessed_lines:
        if index == len(bootcode_list):
            print("The programm was succesfully terminated. The accumulator value is ", accumulator)
            program_terminated.append("finished")
            accessed_lines.append(index)
        else:
            splitted = bootcode_list[index].split()
            action = splitted[0]
            value = int(splitted[1])
            accessed_lines.append(index)
            if action == "nop":
                if nop_count == nops_evaluated:
                    index = index + value
                else:
                    index += 1
                nop_count += 1
            elif action == "acc":
                index += 1
                accumulator += value
            elif action == "jmp":
                index = index + value
    else:
        print("The accumulator before accessing a line for the 2nd time is ", accumulator)
        print("We evaluated nop number ", (nops_evaluated + 1))

while (len(program_terminated) == 0) and (attempt_number < max_attempts):
    debug_bootcode_nop(bootcode_list)
    nops_evaluated += 1
    attempt_number += 1
else:
    print("The programm either finished, or the max. number of attempts was reached.\n\n")
    attempt_number = 1

def debug_bootcode_jmp(mylist):
    index = 0
    accumulator = 0
    accessed_lines = []
    jmp_count = 0   
    while index not in accessed_lines:
        if index == len(bootcode_list):
            print("The programm was succesfully terminated. The accumulator value is ", accumulator)
            program_terminated.append("finished")
            accessed_lines.append(index)
        else:
            splitted = bootcode_list[index].split()
            action = splitted[0]
            value = int(splitted[1])
            accessed_lines.append(index)
            if action == "nop":
                index += 1
            elif action == "acc":
                index += 1
                accumulator += value
            elif action == "jmp":
                if jmp_count == jmps_evaluated:
                    index += 1
                else:
                    index = index + value
                jmp_count += 1
            if (index < 0) or (index > len(bootcode_list)):
                print("The index went out of range. Ending sequence.")
                index = 0
    else:
        print("The accumulator before accessing a line for the 2nd time is ", accumulator)
        print("We evaluated jmp number ", (jmps_evaluated + 1))

while (len(program_terminated) == 0) and (attempt_number < max_attempts):
    debug_bootcode_jmp(bootcode_list)
    jmps_evaluated += 1
    attempt_number += 1
else:
    print("The programm either finished, or the max. number of attempts was reached.\n\n")
    attempt_number = 1
