bags = open(r"C:\Python34\AoC\testfile.txt", "r")

import re

#we need to get:
    #colour of the bag, and the colour of the bag will be equal to:
    #a dictionary of all the contained colours with colour as key and amount as value
#create a dictionary of all the colours as keys and their contents lists as values
#the we can check, for every colour, if it contains the colour we are looking for
dic_of_colours = {}

for line in bags:
    if line != "":
        basic_split = line.split(" bags contain ")
##        print("basic_split", basic_split)
        bag_colour = basic_split[0]
##        print("bag_colour", bag_colour)
        contents = basic_split[1].rstrip(".\n")
##        print("contents", contents)
        listed_contents = contents.split(", ")
##        print("listed_contents", listed_contents)
        dic_of_subcolours = {}
        for index in range(len(listed_contents)):
            content_with_bags = listed_contents[index]
##            print("content_with_bags", content_with_bags)
            content = re.sub(' bags?$', '', content_with_bags)
##            print("content", content)
            if content != "no other":
                first_space_index = content.find(" ")
##                print("first_space_index", first_space_index)
                subbag_amount = int(content[:first_space_index])
##                print("subbag_amount", subbag_amount)
                subbag_colour = (content[(first_space_index):]).strip()
##                print("subbag_colour", subbag_colour)
                dic_of_subcolours[subbag_colour] = subbag_amount
##                print("dic_of_subcolours", dic_of_subcolours)
        dic_of_colours[bag_colour] = dic_of_subcolours
##        print(dic_of_colours)

print("\n\n\nFinal dic of colours:\n", dic_of_colours, "\n\n")

def contains_shiny_gold(colour, data):
    result = False
    for bag_in_bag in data[colour].keys():
        if bag_in_bag == 'shiny gold':
            result = True
        elif contains_shiny_gold(bag_in_bag, data):
            result = True
    return result

def how_many_contain_shiny_gold(data):
    contains_total = 0
    for item in data:
        contains = contains_shiny_gold(item, data)
        if contains == True:
            contains_total += 1
    print("Your list contains", contains_total, "bags with shiny gold bag in them")

how_many_contain_shiny_gold(dic_of_colours)
