#we open our file
passports = open(r"C:\Python34\AoC\passports.txt", "r")
#we read the first line and save it
line = passports.readline()
#we need to collect the individual data and read following related lines,
#then perform a validity check, adjust the total of valid passwords and go to the next section
valid_passports = 0
passports_checked = 0
passport_dic = {}

#first we will create separate functions to check the individual criteria
byr_list = []
for item in range(1920, 2003):
    byr_list.append(str(item))

def valid_byr(passport_dictionary):
    if (("byr" in passport_dictionary) and (passport_dictionary["byr"] in byr_list)):
        return True
    else:
        return False

iyr_list = []
for item in range(2010, 2021):
    iyr_list.append(str(item))

def valid_iyr(passport_dictionary):
    if (("iyr" in passport_dictionary) and (passport_dictionary["iyr"] in iyr_list)):
        return True
    else:
        return False

eyr_list = []
for item in range(2020, 2031):
    eyr_list.append(str(item))

def valid_eyr(passport_dictionary):
    if (("eyr" in passport_dictionary) and (passport_dictionary["eyr"] in eyr_list)):
        return True
    else:
        return False

hgt_list_cm = []
for item in range(150, 194):
    hgt_list_cm.append(str(item))

hgt_list_in = []
for item in range(59, 76):
    hgt_list_in.append(str(item))

def valid_hgt(passport_dictionary):
    if (("hgt" in passport_dic) and ((passport_dic["hgt"][-2:]) == "in") and (passport_dic["hgt"][:-2] in hgt_list_in)):
        return True
    elif (("hgt" in passport_dic) and ((passport_dic["hgt"][-2:]) == "cm") and (passport_dic["hgt"][:-2] in hgt_list_cm)):
        return True
    else:
        return False

hcl_list = ["a", "b", "c", "d", "e", "f"]
for item in range(0, 10):
    hcl_list.append(str(item))

def valid_hcl(passport_dictionary):
    result = True
    if "hcl" in passport_dictionary:
        nohashtag = passport_dictionary["hcl"][1:]
        for char in nohashtag:
            if char not in hcl_list:
                result = False
    if "hcl" not in passport_dictionary:
        return False
    elif passport_dictionary ["hcl"][0] != "#":
        return False
    elif len(passport_dic["hcl"]) != 7:
        return False
    elif result != True:
        return False
    else:
        return True

ecl_list = ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]

def valid_ecl(passport_dictionary):
    if (("ecl" in passport_dictionary) and (passport_dictionary["ecl"] in ecl_list)):
        return True
    else:
        return False

def valid_pid(passport_dictionary):
    if (("pid" in passport_dic) and (passport_dic["pid"].isnumeric()) and (len(passport_dic["pid"]) == 9)):
        return True
    else:
        return False
    
#now for the main code
while (line != ""):
    if (line != "\n"):
        print("accesing line", line.rstrip("\n"))
        list_of_line = line.rstrip("\n").split(" ")
        print("created a list from the line", list_of_line)
        for item in range(len(list_of_line)):
            separated_data = list_of_line[item].split(":")
            print("separated the data", separated_data)
            key = separated_data[0]
            print("isolated key", key)
            data = separated_data[1]
            print("isolated data", data)
            passport_dic[key] = data
            print("updated password dic to", passport_dic)
        line = passports.readline()
        print("\n")
    else:
        print("validating passport dic", passport_dic)
        print("byr is", valid_byr(passport_dic), "and iyr is", valid_iyr(passport_dic), "and eyr is", valid_eyr(passport_dic), "and hgt is", valid_hgt(passport_dic), "and hcl is", valid_hcl(passport_dic), "and ecl is", valid_ecl(passport_dic), "and pid is", valid_pid(passport_dic))
        if valid_byr(passport_dic) and valid_iyr(passport_dic) and valid_eyr(passport_dic) and valid_hgt(passport_dic) and valid_hcl(passport_dic) and valid_ecl(passport_dic) and valid_pid(passport_dic):
            valid_passports += 1
            print("\n\nTOTAL passwords increased to", valid_passports)
        else:
            print("\n\nTOTAL passwords not increased, password is invalid, value remains at", valid_passports)
        passports_checked += 1
        print("Checked", passports_checked, "passports so far.\n\n")
        passport_dic = {}
        line = passports.readline()

#last passport has to be checked
print("validating passport dic", passport_dic)
print("byr is", valid_byr(passport_dic), "and iyr is", valid_iyr(passport_dic), "and eyr is", valid_eyr(passport_dic), "and hgt is", valid_hgt(passport_dic), "and hcl is", valid_hcl(passport_dic), "and ecl is", valid_ecl(passport_dic), "and pid is", valid_pid(passport_dic))
if valid_byr(passport_dic) and valid_iyr(passport_dic) and valid_eyr(passport_dic) and valid_hgt(passport_dic) and valid_hcl(passport_dic) and valid_ecl(passport_dic) and valid_pid(passport_dic):
    valid_passports += 1
    print("\n\nTOTAL passwords increased to", valid_passports)
else:
    print("\n\nTOTAL passwords not increased, password is invalid, value remains at", valid_passports)
passports_checked += 1
print("Checked", passports_checked, "passports in total.\n\n")

print("Dear Jakub, the number of all valid passports is:", valid_passports, "\n\n")
passports.close()
#done
