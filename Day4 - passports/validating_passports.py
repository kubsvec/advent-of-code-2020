#we open our file
passports = open(r"C:\Python34\AoC\passports.txt", "r")
#we read the first line and save it
line = passports.readline()
#we need to collect the individual data and read following related lines,
#then perform a validity check, adjust the total of valid passwords and go to the next section
total_passwords = 0
passports_checked = 0
password_list = []

while (line != ""):
    if (line != "\n"):
        print("accesing line", line)
        list_of_line = line.rstrip("\n").split(" ")
        print("created a list from the line", list_of_line)
        for item in range(len(list_of_line)):
            key = list_of_line[item][0:3]
            print("isolated key", key)
            password_list.append(key)
            print("updated password list to", password_list)
        line = passports.readline()
    else:
        print("applying password list", password_list)
        if ("byr" in password_list) and ("iyr" in password_list) and ("eyr" in password_list) and ("hgt" in password_list) and ("hcl" in password_list) and ("ecl" in password_list) and ("pid" in password_list):
            total_passwords += 1
            print("\n\nTOTAL passwords increased to", total_passwords)
        else:
            print("\n\nTOTAL passwords not increased, password is invalid.")
        passports_checked += 1
        print("Checked", passports_checked, "passports so far.\n\n")
        password_list = []
        line = passports.readline()

print("applying password list", password_list)
if ("byr" in password_list) and ("iyr" in password_list) and ("eyr" in password_list) and ("hgt" in password_list) and ("hcl" in password_list) and ("ecl" in password_list) and ("pid" in password_list):
    total_passwords += 1
    print("\n\nTOTAL passwords increased to", total_passwords, "\n\n")
else:
    print("\n\nTOTAL passwords not increased, password is invalid.")
passports_checked += 1
print("Checked", passports_checked, "passports in total.\n\n")

print("Dear Jakub, the number of valid passports is:", total_passwords, "\n\n")
passports.close()
