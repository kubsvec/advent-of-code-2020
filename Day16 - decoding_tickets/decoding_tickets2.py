tickets = open(r"C:\Python34\Advent of Code 2020\Day16 - decoding_tickets\valid_tickets.txt", "r")

def check_keyword(word, dictionary, removing, index, limit):
    index += 1
    next_words = []
    for dicitem in dictionary[index]:
        next_words.append(dicitem)
    for each in removing:
        if each in next_words:
            next_words.remove(each)
    if len(next_words) == 1:
        if index < (limit - 1):
            new_word = next_words[0]
            removing.append(new_word)
            check_keyword(new_word, dictionary, removing, index, limit)
        elif index == (limit - 1):
            print("Found the right branch!")
            removing.append(next_words[0])
            print(removing)
    elif len(next_words) > 1:
        for every in next_words:
            while len(removing) > index:
                removing.remove(removing[-1])
            else:
                myword = every
                removing.append(every)
                check_keyword(every, dictionary, removing, index, limit)

def find_positions(word, dictionary):
    summary = []
    for index in range(20):
        if word in dictionary[index]:
            summary.append(index)
    print(word, "can be found at indices", summary)

def evaluate_tickets(tickets):
    #first we need to get the criteria from a file open for reading
    criteria = {}
    keys = []
    line = tickets.readline()
    while line != "\n":
        line = line.rstrip("\n")
        line = line.split(": ")
        key = line[0]
        keys.append(key)
        line = line[1]
        line = line.split(" ")
        range1 = line[0]
        range1 = range1.split("-")
        range2 = line[2]
        range2 = range2.split("-")
        value = []
        for char in range(int(range1[0]), (int(range1[1]) + 1)):
            if char not in value:
                value.append(char)
        for char in range(int(range2[0]), (int(range2[1]) + 1)):
            if char not in value:
                value.append(char)
        criteria[key] = value
        line = tickets.readline()
    else:
##        print("Criteria:", criteria)
        print("Keys in their correct order", keys)
        line = tickets.readline()
        line = tickets.readline()
    #then we create a list of values in our ticket
        line = line.rstrip("\n")
        my_ticket = line.split(",")
        print("My ticket is:", my_ticket)
        line = tickets.readline()
        line = tickets.readline()
        line = tickets.readline()
    #now we start checking the tickets
    how_many = len(keys)
    #for every position, we create a list of all occuring values
    values = {}
    for index in range(how_many):
        key = index
        values[key] = []
    while line != "":
        line = line = line.rstrip("\n")
        line = line.split(",")
        initial = 0
        for number in line:
            values[initial].append(int(number))
            initial += 1
        line = tickets.readline()
    else:
##        print("Positions with their occuring values:", values)
        print("Checkpoint.")
    #now we need to pair up our criteria with tickets
    finaldic = {}
    for index in range(how_many):
        hlasky = []
        for key in keys:
            validator = True
            for number in values[index]:
                if number not in criteria[key]:
                    validator = False
            if validator == True:
                hlasky.append(key)
        finaldic[index] = hlasky
    print("Final dictionary is:", finaldic)
    for key in keys:
        counter = 0
        for index in range(how_many):
            if key in finaldic[index]:
                counter += 1
        print("Occurence of", key, "is", counter, "times in total.")
    #now we try to figure out a valid combination

##    for first in finaldic[0]:
##        print("Checking with", first, "as the first option in our branch.")
##        removelist = [first]
##        check_keyword(first, finaldic, removelist, 0, how_many)
    for word in keys:
        find_positions(word, finaldic)
