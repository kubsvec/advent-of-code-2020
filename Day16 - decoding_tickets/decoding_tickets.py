tickets = open(r"C:\Python34\Advent of Code 2020\Day16 - decoding_tickets\tickets.txt", "r")

def evaluate_tickets(tickets):
    #first we need to get the criteria from a file open for reading
    criteria = []
    line = tickets.readline()
    while line != "\n":
        line = line.rstrip("\n")
        line = line.split(": ")
        line = line[1]
        line = line.split(" ")
        range1 = line[0]
        range1 = range1.split("-")
        range2 = line[2]
        range2 = range2.split("-")
        for char in range(int(range1[0]), (int(range1[1]) + 1)):
            if char not in criteria:
                criteria.append(char)
        for char in range(int(range2[0]), (int(range2[1]) + 1)):
            if char not in criteria:
                criteria.append(char)
        line = tickets.readline()
    else:
        criteria.sort()
        print("Criteria:", criteria)
        line = tickets.readline()
    #then we skip our own ticket and load the first line of nearby tickets
    while line != "\n":
        line = tickets.readline()
    else:
        line = tickets.readline()
        line = tickets.readline()
    #now we start checking the tickets and adding the invalid numbers to our solution
    solution = 0
    while line != "":
        sline = line.rstrip("\n")
        sline = sline.split(",")
        validator = True
        for number in sline:
            if int(number) not in criteria:
                solution += int(number)
                validator = False
        if validator == True:
            print(line.rstrip("\n"))
        line = tickets.readline()
    else:
        print("Solution is:", solution)
        tickets.close()
