directions = open(r"C:\Python34\Advent of Code 2020\Day12 - ferry_directions\instructions.txt", "r")

waypoint1 = [10, 0]
waypoint2 = [1, 270]

north_counter = 0
east_counter = 0

def updatewaypoint(instruction, units):
    """
    "E" = 0
    "S" = 90
    "W" = 180
    "N" = 270
    """
    if instruction == "L":
        units = - units
    change1 = (waypoint1[1] + units) % 360
    change2 = (waypoint2[1] + units) % 360
    waypoint1[1] = change1
    waypoint2[1] = change2

"""
N = move waypoint north
S = move waypoint south
E = move waypoint east
W = move waypoint west
L = turn waypoint left (-degrees)
R = turn waypoint right (+degrees)
F = move ship times waypoint
"""

for line in directions:
    line.rstrip("\n")
    instruction = line[0]
    units = int(line[1:])
    if instruction == "N":
        if waypoint1[1] == 270:
            waypoint1[0] += units
        elif waypoint2[1] == 270:
            waypoint2[0] += units
        elif waypoint1[1] == 90:
            waypoint1[0] -= units
        elif waypoint2[1] == 90:
            waypoint2[0] -= units
        else:
            print("NO right direction!")
    if instruction == "S":
        if waypoint1[1] == 90:
            waypoint1[0] += units
        elif waypoint2[1] == 90:
            waypoint2[0] += units
        elif waypoint1[1] == 270:
            waypoint1[0] -= units
        elif waypoint2[1] == 270:
            waypoint2[0] -= units
        else:
            print("NO right direction!")
    if instruction == "E":
        if waypoint1[1] == 0:
            waypoint1[0] += units
        elif waypoint2[1] == 0:
            waypoint2[0] += units
        elif waypoint1[1] == 180:
            waypoint1[0] -= units
        elif waypoint2[1] == 180:
            waypoint2[0] -= units
        else:
            print("NO right direction!")
    if instruction == "W":
        if waypoint1[1] == 180:
            waypoint1[0] += units
        elif waypoint2[1] == 180:
            waypoint2[0] += units
        elif waypoint1[1] == 0:
            waypoint1[0] -= units
        elif waypoint2[1] == 0:
            waypoint2[0] -= units
        else:
            print("NO right direction!")
    if instruction == "L":
        updatewaypoint(instruction, units)
    if instruction == "R":
        updatewaypoint(instruction, units)
    if instruction == "F":
        if waypoint1[1] == 0:
            east_counter += (units * waypoint1[0])
        elif waypoint1[1] == 180:
            east_counter -= (units * waypoint1[0])
        elif waypoint1[1] == 270:
            north_counter += (units * waypoint1[0])
        elif waypoint1[1] == 90:
            north_counter -= (units * waypoint1[0])
        if waypoint2[1] == 0:
            east_counter += (units * waypoint2[0])
        elif waypoint2[1] == 180:
            east_counter -= (units * waypoint2[0])
        elif waypoint2[1] == 270:
            north_counter += (units * waypoint2[0])
        elif waypoint2[1] == 90:
            north_counter -= (units * waypoint2[0])

print("We ended up", north_counter, "units to the north and", east_counter, "units to the east.")

count_manhattan = abs(north_counter) + abs(east_counter)

print("Final Manhattan distance is", count_manhattan)
