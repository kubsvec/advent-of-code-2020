directions = open(r"C:\Python34\Advent of Code 2020\Day12 - ferry_directions\instructions.txt", "r")

direction = [0]
north_counter = 0
east_counter = 0

"""
0 = east
90 = south
180 = west
270 = north
"""

def updatedirection(instruction, units):
    direct = (direction[0])
    if instruction == "L":
        units = - units
    newdirection = (direct + units) % 360
    direction[0] = newdirection

def setdirection(direction):
    units = direction[0]
    if units == 0:
        return "E"
    elif units == 90:
        return "S"
    elif units == 180:
        return "W"
    elif units == 270:
        return "N"
    else:
        print("INVALID DIRECTION")

"""
N = move north
S = move south
E = move east
W = move west
L = turn left (-degrees)
R = turn right (+degrees)
F = move forward
"""

for line in directions:
    line.rstrip("\n")
    instruction = line[0]
    units = int(line[1:])
    if instruction == "F":
        instruction = setdirection(direction)
    if instruction == "N":
        north_counter += units
    if instruction == "S":
        north_counter -= units
    if instruction == "E":
        east_counter += units
    if instruction == "W":
        east_counter -= units
    if instruction == "L":
        updatedirection(instruction, units)
    if instruction == "R":
        updatedirection(instruction, units)

print("We ended up", north_counter, "units to the north and", east_counter, "units to the east.")

count_manhattan = abs(north_counter) + abs(east_counter)

print("Final Manhattan distance is", count_manhattan)
