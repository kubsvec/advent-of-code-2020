seats = open(r"C:\Python34\Advent of Code 2020\Day11 - occupied_seats\seats.txt", "r")

#first we create a dictionary where keys will be numbers of rows and values will be lists of seats
#we surround the original with tiles "okraj"

seatdic = {}

def fill_seatdic(openfile):
    index = 1
    length = 0
    for everyline in seats:
        line = everyline.rstrip("\n")
        length = len(line)
        linelist = ["o"]
        for char in line:
            linelist.append(char)
        linelist.append("o")
        seatdic[index] = linelist
        index += 1
    extraline = (length + 2) * "o"
    extralist = []
    for char in extraline:
        extralist.append(char)
    seatdic[0] = extralist
    seatdic[index] = extralist

fill_seatdic(seats)
print("Our seatdic:", seatdic)
print("\nNow run the functions update_seatdic(seatdic) repeatedly, until the value of seats_changed will stay at 0.")

rows = len(seatdic)
linelength = len(seatdic[0])

def findleft(row, column):
    nextseat = seatdic[row][column - 1]
    if nextseat in ["o", "#", "L"]:
        return nextseat
    else:
        nextseat = findleft(row, column - 1)
    return nextseat

def findright(row, column):
    nextseat = seatdic[row][column + 1]
    if nextseat in ["o", "#", "L"]:
        return nextseat
    else:
        nextseat = findright(row, column + 1)
    return nextseat

def findup(row, column):
    nextseat = seatdic[row + 1][column]
    if nextseat in ["o", "#", "L"]:
        return nextseat
    else:
        nextseat = findup(row + 1, column)
    return nextseat

def finddown(row, column):
    nextseat = seatdic[row - 1][column]
    if nextseat in ["o", "#", "L"]:
        return nextseat
    else:
        nextseat = finddown(row - 1, column)
    return nextseat

def findleftup(row, column):
    nextseat = seatdic[row + 1][column - 1]
    if nextseat in ["o", "#", "L"]:
        return nextseat
    else:
        nextseat = findleftup(row + 1, column - 1)
    return nextseat

def findleftdown(row, column):
    nextseat = seatdic[row - 1][column - 1]
    if nextseat in ["o", "#", "L"]:
        return nextseat
    else:
        nextseat = findleftdown(row - 1, column - 1)
    return nextseat

def findrightup(row, column):
    nextseat = seatdic[row + 1][column + 1]
    if nextseat in ["o", "#", "L"]:
        return nextseat
    else:
        nextseat = findrightup(row + 1, column + 1)
    return nextseat

def findrightdown(row, column):
    nextseat = seatdic[row - 1][column + 1]
    if nextseat in ["o", "#", "L"]:
        return nextseat
    else:
        nextseat = findrightdown(row - 1, column + 1)
    return nextseat

#now we need a function, that will assess every tile and look for seats
def update_seatdic(seatdic):
    seats_changed = 0
    newdic = {}
    for num in range(0, rows):
        newdic[num] = []
        while len(newdic[num]) < linelength:
            newdic[num].append("o")
    for key in range(1, (rows - 1)):
        for index in range(1, (linelength - 1)):
            seat = seatdic[key][index]
            adjacent_seats = [findleft(key, index), findright(key, index), findup(key, index), finddown(key, index), findrightup(key, index), findrightdown(key, index), findleftup(key, index), findleftdown(key, index)]
            if (seat == "L") and ("#" not in adjacent_seats):
                newdic[key][index] = "#"
                seats_changed += 1
            elif (seat == "L") and ("#" in adjacent_seats):
                newdic[key][index] = "L"
            elif (seat == "#") and (adjacent_seats.count("#") >= 5):
                newdic[key][index] = "L"
                seats_changed += 1
            elif (seat == "#") and (adjacent_seats.count("#") < 5):
                newdic[key][index] = "#"
            elif (seat == "."):
                newdic[key][index] = "."
    print(seats_changed, "seats have changed their status. New seatdic not printed to save memory.")
##    print(newdic)
    for item in newdic.keys():
        index = 0
        for subitem in newdic[item]:
            seatdic[item][index] = subitem
            index += 1

def count_occupied(seatdic):
    occupied = 0
    for item in seatdic.keys():
        index = 0
        for subitem in seatdic[item]:
            if seatdic[item][index] == "#":
                occupied += 1
            index += 1
    print(occupied, "seats are occupied.")

"""
. = floor
#  = occupied
L = empty

adjacent seats = the 8 seats surrounding a seat

- If a seat is empty (L) and there are no occupied seats adjacent to it, the seat becomes occupied.
- If a seat is occupied (#) and four or more seats adjacent to it are also occupied, the seat becomes empty.
- Otherwise, the seat's state does not change.
"""
