def get_number(speaktimes, inputlist):#(2020, [0, 3, 6])
    spoken = len(inputlist)#3
    repeats = range(len(inputlist), (speaktimes - 1))#range(3, 2019)
    last_num = inputlist[-1]#6
    where_last = {}
    for index in range(len(inputlist) - 1):
        where_last[inputlist[index]] = index + 1
    #{0:1, 3:2}
    for every in repeats:
        if last_num in where_last:
            difference = spoken - where_last[last_num]
            where_last[last_num] = spoken
            last_num = difference
        else:
            where_last[last_num] = spoken
            last_num = 0
        spoken += 1
    #last number with printing the result
    if last_num in where_last:
        difference = spoken - where_last[last_num]
        print("Last spoken number at position", speaktimes, "is", difference)
    else:
        print("Last spoken number at position", speaktimes, "is 0")
