#input: 0,14,1,3,7,9

starting = [0, 14, 1, 3, 7, 9]
gamedic = {}
spoken_twice = [0]
for index in range(len(starting)):
    gamedic[starting[index]] = index + 1
        
print(gamedic)
current = len(starting) + 1
last_num = starting[-1]

amount = (99999 - len(starting))

def get_next(amount, current, last_num):
    for num in range(amount):
        last = last_num
        if last not in spoken_twice:
##            print("speaking", current, "times, last number was", last, "so I say", 0)
            last_num = 0
            gamedic[last] = current - 1
##            print("updated starting to", starting)
##            print("updated gamedic to", gamedic)
        else:
            difference = (current - 1) - (gamedic[last])
##            print("speaking", current, "times, last number was", last, "so I say", difference)
            if difference in gamedic:
                spoken_twice.append(difference)
            last_num = difference
            gamedic[last] = current - 1
##            print("updated starting to", starting)
##            print("updated gamedic to", gamedic)
        current += 1
    for num in range(1):
        last = last_num
        if last not in spoken_twice:
            last_num = 0
            gamedic[last] = current - 1
            print("speaking", current, "times, last number was", last, "so I say", 0)
        else:
            difference = (current - 1) - (gamedic[last])
            if difference in gamedic:
                spoken_twice.append(difference)
            last_num = difference
            gamedic[last] = current - 1
            print("speaking", current, "times, last number was", last, "so I say", difference)
