adapters = open(r"C:\Python34\Advent of Code 2020\Day10 - charging_adapters\adapters.txt", "r")
#we put 0 into the first list to represent the socket
data = []
#we add all adapters into the list
for line in adapters:
    value = int(line.strip())
    data.append(value)
#we sort the lists from the smallest to the biggest adapter

def star2(data):
    """
    >>> star2([1, 4, 5, 6])
    2

    >>> star2([16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4])
    8

    >>> star2([28, 33, 18, 42, 31, 14, 46, 20, 48, 47, 24, 23, 49, 45, 19, 38, 39, 11, 1, 32, 25, 35, 8, 17, 7, 9, 4, 2, 34, 10, 3])
    19208
    """

    data.sort()
    cache = {}

    return no_combinations(0, 0, max(data) + 3, data, cache)


def no_combinations(joltage, index, device_joltage, data, cache):
    key = '%d-%d' % (joltage, index)
    if key in cache:
        return cache[key]

    result = 0

    while index < len(data) and data[index] <= joltage + 3:
        result += no_combinations(data[index], index + 1, device_joltage, data, cache)
        index += 1

    if joltage + 3 >= device_joltage:
        result += 1

    cache[key] = result
    return result
