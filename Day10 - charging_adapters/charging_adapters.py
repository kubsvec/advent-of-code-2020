adapters = open(r"C:\Python34\Advent of Code 2020\Day10 - charging_adapters\adapters.txt", "r")
#we put 0 into the first list to represent the socket
adapterlist = [0]
adapterlist_nozeros = []
#we add all adapters into the list
for line in adapters:
    value = int(line.strip())
    adapterlist.append(value)
    adapterlist_nozeros.append(value)
#we sort the lists from the smallest to the biggest adapter
adapterlist.sort()

def count_voltages(alist):
    difference_one = 0
    difference_two = 0
    #difference_three starts at one because my device is 3 jots higher than the highest adapter
    difference_three = 1
    for index in range(0, (len(alist) - 1)):
        difference = alist[(index + 1)] - alist[index]
        if difference == 1:
            difference_one += 1
        elif difference == 2:
            difference_two += 1
        elif difference == 3:
            difference_three += 1
        elif difference > 3:
            print("Careful, all adapters do not match!")
    print("Difference of 1 jolt occurs", difference_one, "times in total")
    print("Difference of 2 jolts occurs", difference_two, "times in total")
    print("Difference of 3 jolts occurs", difference_three, "times in total\n\n")

count_voltages(adapterlist)
minimal = 0
maximal = max(adapterlist_nozeros)
minlist = [(min(adapterlist_nozeros) - 3), (min(adapterlist_nozeros) - 6)]
maxlist = [(max(adapterlist_nozeros) + 3), (max(adapterlist_nozeros) + 6), (max(adapterlist_nozeros) + 9)]
adapterlist_nozeros.append(minlist[0])
adapterlist_nozeros.append(minlist[1])
adapterlist_nozeros.append(0)
adapterlist_nozeros.append(maxlist[0])
adapterlist_nozeros.append(maxlist[1])
adapterlist_nozeros.append(maxlist[2])
adapterlist_nozeros.sort()

print("No zero adapter list, true list begins with", minimal, "and ends with", maximal)
print(adapterlist_nozeros, "\n\n")

def can_be_skipped(alist):
    can_skip_numbers = []
    can_skip_doubles = []
    can_skip_triples = []
    can_skip_solo = []
    for index in range(3, (len(alist) -4)):
        if (alist[index] - alist[index - 1] < 3) and (alist[index + 1] - alist[index] < 3):
            can_skip_numbers.append(alist[index])
    for index in range((len(can_skip_numbers) - 1)):
        if can_skip_numbers[index] == (can_skip_numbers[index + 1] - 1):
            can_skip_doubles.append([can_skip_numbers[index], can_skip_numbers[index + 1]])
    for index in range((len(can_skip_numbers) - 2)):
        if (can_skip_numbers[index] == (can_skip_numbers[index + 1] - 1)) and (can_skip_numbers[index] == (can_skip_numbers[index + 2] - 2)):
            can_skip_triples.append([can_skip_numbers[index], can_skip_numbers[index + 1], can_skip_numbers[index + 2]])
    doubles_amount = len(can_skip_doubles)
    for number in can_skip_numbers:
        included = False
        for notindex in range(doubles_amount):
            if number in can_skip_doubles[notindex]:
                included = True
        if not included:
            can_skip_solo.append(number)
    print("Skipped numbers:", can_skip_numbers)
    print("Skipped doubles:", can_skip_doubles)
    print("Skipped triples:", can_skip_triples)
    print("Skipped numbers solo:", can_skip_solo)
    print("\nThere are", len(can_skip_solo), "solo singles and", (doubles_amount - (len(can_skip_triples) * 2)), "solo doubles and", len(can_skip_triples), "triples in total.")

can_be_skipped(adapterlist_nozeros)

print("""Please run function now:
count_options(number of solo singles, number of solo doubles, number of triples)""")

def count_options(single, double, triple):
    options = 1 * (4 ** double) * (2 ** single) * (7 ** triple)
    print(options, "options are available.")
