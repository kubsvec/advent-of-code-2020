#we open the forest file
forest = open(r"C:\Python34\AoC\toboggan_forest.txt", "r")
#we read the first line and save it for analysis
row = forest.readline()
#we determine the length of one row without the newline character
length1 = len(row.rstrip("\n"))
#now we have the number of characters in a row after which they repeat
#we set the default slope index to 0 for our first line and the jump to 3
slope = 0
slope_jump = 3
#now we need a function that will keep reading lines until we reach the end
#of the file and perform checks for trees at changing indices, and add trees to the total
total_trees = 0

while row != "":
    if row[slope] == '#':
        total_trees += 1
#    print(row, slope, row[slope], total_trees)
    slope = (slope + slope_jump) % length1
    row = forest.readline()

print("total trees =", total_trees)
